Option Explicit

Private searchBoxes(1 To 5) As MSForms.TextBox
Private boolBoxes(1 To 4) As ComboBox
Private checkBoxes(1 To 3) As MSForms.CheckBox

Private maxCol As Integer
Private notesCol As Integer
Private lastRow As Integer
Private mainSheetName As String
Private createdSheets As New Collection


'//////////////////////////////
'sets up arrays and combo boxes
Public Sub SetUp()

MainSheetNameBox.text = "UA_Home_Version"
Call SetSearchSheetButton_Click

Set boolBoxes(1) = BoolBox1
Set boolBoxes(2) = BoolBox2
Set boolBoxes(3) = BoolBox3
Set boolBoxes(4) = BoolBox4

Set searchBoxes(1) = CommandForm.SearchBox1
Set searchBoxes(2) = CommandForm.SearchBox2
Set searchBoxes(3) = CommandForm.SearchBox3
Set searchBoxes(4) = CommandForm.SearchBox4
Set searchBoxes(5) = CommandForm.SearchBox5

Set checkBoxes(1) = CommandForm.SearchCheckBox
Set checkBoxes(2) = ColumnSearchCheckBox
Set checkBoxes(3) = DateCheckBox

Dim cBox
For Each cBox In boolBoxes
    cBox.AddItem ("And")
    cBox.AddItem ("Not")
    cBox.AddItem ("Or")
    cBox.AddItem ("Xor")
Next cBox

ColumnBoolBox.AddItem ("in")
ColumnBoolBox.AddItem ("not in")
ColumnBoolBox.ListIndex = 0

DateBoolBox.AddItem ("after")
DateBoolBox.AddItem ("before")
DateBoolBox.ListIndex = 0

Me.StartUpPosition = 0
Me.Top = 0
Me.Left = Application.Width - Me.Width + Application.Left
'MsgBox ("setup has run")
SearchCheckBox.Value = True

Call SetAvailableFocus

End Sub

'//////////////////////////////
'zero out search fields
Private Sub ClearButton_Click()

Dim thing
For Each thing In searchBoxes
    thing.text = ""
Next thing
For Each thing In boolBoxes
    thing.ListIndex = -1
Next thing


'there weren't enough additional fields to for me
'to feel the need to put them in an array to loop
'through.  if more are added, i'd do it
ColumnTermBox.text = ""
DateMonthBox.text = ""
DateYearBox.text = ""

Call SetAvailableFocus

End Sub

'//////////////////////////////
Private Sub ColumnSearchCheckBox_Click()

ColumnTermBox.Enabled = ColumnSearchCheckBox.Value
ColumnBoolBox.Enabled = ColumnSearchCheckBox.Value
ColumnColBox.Enabled = ColumnSearchCheckBox.Value

End Sub

'//////////////////////////////
Private Sub DateCheckBox_Click()

DateBoolBox.Enabled = DateCheckBox.Value
DateMonthBox.Enabled = DateCheckBox.Value
DateYearBox.Enabled = DateCheckBox.Value

End Sub

'//////////////////////////////
Private Sub SearchCheckBox_Click()

Dim thing
For Each thing In boolBoxes
    thing.Enabled = SearchCheckBox.Value
Next thing

For Each thing In searchBoxes
    thing.Enabled = SearchCheckBox.Value
Next thing

End Sub

'//////////////////////////////
Private Sub SetSearchSheetButton_Click()

'check if the sheet exists
If Not WorksheetExists(MainSheetNameBox.text) Then
    MsgBox ("Sheet Does Not Exist")
    Exit Sub
End If

'reset variables used to control the search functions
mainSheetName = MainSheetNameBox.text
notesCol = FindColNum("Notes")
maxCol = FindColNum("") - 1
lastRow = ThisWorkbook.Sheets(mainSheetName).UsedRange.rows.count

Do While ColumnColBox.ListCount > 0
    ColumnColBox.RemoveItem (0)
Loop

Dim i As Integer
For i = 1 To maxCol
    ColumnColBox.AddItem (ThisWorkbook.Sheets(mainSheetName).Cells(1, i).Value)
Next i

Call SetAvailableFocus

End Sub

'//////////////////////////////
Private Sub DeleteButton_Click()

Dim thing
For Each thing In createdSheets
    If WorksheetExists(thing) Then
        ThisWorkbook.Sheets(thing).Delete
    End If
Next thing

End Sub

'//////////////////////////////
'find number of a column by first row cell text
Function FindColNum(thing As String) As Integer

Dim returnValue As Integer
returnValue = 1
Do While Trim(LCase(ThisWorkbook.Sheets(mainSheetName).Cells(1, returnValue).text)) <> LCase(thing)
    'catch if there is no "notes" column
    If returnValue = 16384 Then
        MsgBox ("the first row does not have a cell containing " & thing & vbCrLf & "this searching thing will not work without it" & vbCrLf & " sorry")
        End
    End If
    returnValue = returnValue + 1
Loop
FindColNum = returnValue

End Function

'//////////////////////////////
Private Sub SearchButton_Click()

Dim name As String
name = CreateWorksheet

'CreateWorksheet is used to determine if the search should continue
If name = "" Then
    Exit Sub
End If

Dim i As Integer
Dim currentPasteRow As Integer
currentPasteRow = 2
For i = 1 To lastRow
    'if any of the search boxes is unchecked, that search returns true
    If SearchNotesCheck(i) And SearchColumnCheck(i) And SearchDateCheck(i) Then
        Call CopyToWorksheet(i, currentPasteRow, name)
        currentPasteRow = currentPasteRow + 1
    End If
Next i

End Sub

'//////////////////////////////
'returns number of search boxes used.  will only count consecutive boxes, starting
'with the top box
Private Function GetNumberOfSearchTerms() As Integer

Dim returnValue As Integer
returnValue = 0

Dim thing
For Each thing In searchBoxes
    'stops counting as soon as it encounters an empty search field
    If thing.text = "" Then
    Exit For
    Else
        returnValue = returnValue + 1
    End If
Next thing
    
GetNumberOfSearchTerms = returnValue

End Function

'//////////////////////////////
'returns a string array of the search terms
Private Function GetSearchTerms(number As Integer, values() As MSForms.TextBox)

Dim returnValue() As String
ReDim returnValue(1 To number)
Dim start As Integer
For start = 1 To number
    returnValue(start) = values(start).Value
Next start

GetSearchTerms = returnValue

End Function

'//////////////////////////////
'returns a string array of operators from combo boxes
Private Function GetLogicOperators(number As Integer, values() As ComboBox)

Dim returnValue() As String
ReDim returnValue(1 To number - 1)
Dim start As Integer
For start = 1 To number - 1
    returnValue(start) = values(start).Value
Next start

GetLogicOperators = returnValue

End Function

'//////////////////////////////
'use a combo box value to chose a logic operator and use it on two boolean values.
'a blank operator is treated as And
Private Function LogicCheck(searchBools() As Boolean, operators() As String) As Boolean

Dim b1 As Boolean
Dim b2 As Boolean
Dim currentOperator As String
b1 = searchBools(UBound(searchBools))
currentOperator = operators(UBound(operators))

'if there is only one search term left, recursion stops
If UBound(searchBools) = 2 Then
    b2 = searchBools(1)
Else
    'truncated arrays are passed forward
    'the values are stripped in reverse order
    'but should be processed correctly due
    'to the nature of recursion
    ReDim Preserve searchBools(1 To UBound(searchBools) - 1)
    ReDim Preserve operators(1 To UBound(operators) - 1)
    b2 = LogicCheck(searchBools, operators)
End If

    Select Case currentOperator
        Case "And"
            LogicCheck = b2 And b1
        Case ""
            LogicCheck = b2 And b1
        Case "Not"
            LogicCheck = b2 And Not b1
        Case "Or"
            LogicCheck = b2 Or b1
        Case "Xor"
            LogicCheck = b2 Xor b1
        Case Else
            LogicCheck = False
            MsgBox ("logicCheck error" + vbCrLf + "check logic operators")
            End
    End Select

End Function

'//////////////////////////////
'returns whether or not search term is contained in a Notes cell
'can take a string or regex
Function SearchForTerm(row As Integer, column As Integer, term As Variant) As Boolean

Dim content As String
content = ThisWorkbook.Sheets(mainSheetName).Cells(row, column).Value

Select Case TypeName(term)
    Case "IRegExp2"
        SearchForTerm = term.Test(content)
    Case "String"
        SearchForTerm = InStr(1, content, term, vbTextCompare) <> 0
    Case Else
        MsgBox ("something went wrong in SearchForTerm")
        SearchForTerm = False
        End
End Select

End Function

'//////////////////////////////
'creates a worksheet, gives it a name based on the search and logic fields, returns the name
'CreateWorksheet also checks to make sure valid search data is present; i should probably separate
'those into two separate functions at some point, but it's just so convenient right now
Function CreateWorksheet() As String

Dim i As Integer
Dim keepGoing As Boolean
keepGoing = False
'make sure at least one search option is active
For i = 1 To UBound(checkBoxes)
    If checkBoxes(i).Value Then
        keepGoing = True
    End If
Next i

'first exit point, if no search options are checked
If Not keepGoing Then
    CreateWorksheet = ""
    Exit Function
End If
    
Dim name As String

'if search-by-term is active, then the search terms are added to the
'name of the new sheet
If SearchCheckBox.Value Then
    For i = 1 To UBound(searchBoxes) - 1 'was hardcoded to 4
        name = name + searchBoxes(i).text + " " + boolBoxes(i).text + " "
    Next i
    name = Trim(name + searchBoxes(UBound(searchBoxes))) 'was hardcoded to 5
    'second exit point, if the search-notes box is checked, but no search terms are entered
    If name = "" Then
        CreateWorksheet = ""
        MsgBox ("no terms entered in 'Search by Term'")
        Exit Function
    End If
End If

'if column search is checked, its terms are added to the name
If ColumnSearchCheckBox.Value Then
    'third exit point, if column search fields are not complete
    If ColumnTermBox.text = "" Or ColumnBoolBox.ListIndex = -1 Or ColumnColBox.ListIndex = -1 Then
        CreateWorksheet = ""
        MsgBox ("something is missing from column search")
        Exit Function
    End If
    'if search-by-term and column search are active,
    'then 'with' is added to join the names
    If SearchCheckBox.Value Then
        name = name & " with "
    End If
    name = name & ColumnTermBox.text & " " & ColumnBoolBox.Value & " " & UCase(ColumnColBox.Value)
End If

'if date search is checked, its terms are aded to the name
If DateCheckBox.Value Then
    If DateBoolBox.ListIndex = -1 Or Not DateDataCheck(DateMonthBox.text, 0, 13) Or Not DateDataCheck(DateYearBox.text, 0) Then
        CreateWorksheet = ""
        MsgBox ("something is wrong with search by date")
        Exit Function
    End If
    If SearchCheckBox.Value Or ColumnSearchCheckBox.Value Then
        name = name & " "
    End If
    name = name & DateBoolBox.Value & " " & DateMonthBox.text & "-" & DateYearBox.text
End If

'keeps the sheet name at a length excel will tolerate
If Len(name) > 31 Then
    name = Left(name, 26) & "-cont"
End If

'if a worksheet with the current name already exists, its
'contents are deleted and replaced.  because the sheet name
'is build from the search description and can be truncated,
'different searches can generate the same sheet name
If WorksheetExists(name) Then
    Dim thing
    Dim deletable As Boolean
    deletable = False
    For Each thing In createdSheets
        If thing = name Then
            deletable = True
            Exit For
        End If
    Next thing
    If deletable Then
        ThisWorkbook.Sheets(name).Cells.Clear
    Else
        MsgBox ("cannot perform search, worksheet name is taken by something that can't be deleted" & vbCrLf & "you will need to find the offending sheet and delete it manually" & vbCrLf & "worksheet name: " & name)
        CreateWorksheet = ""
        Exit Function
    End If
Else
    Sheets.Add.name = name
End If
'fills in the header row
For i = 1 To maxCol
    ThisWorkbook.Sheets(mainSheetName).Cells(1, i).Copy
    ThisWorkbook.Sheets(name).Cells(1, i).PasteSpecial Paste:=xlPasteValues
    ThisWorkbook.Sheets(name).Cells(1, i).PasteSpecial Paste:=xlPasteFormats
    ThisWorkbook.Sheets(name).Cells(1, i).PasteSpecial Paste:=xlPasteColumnWidths
Next i
'End If

createdSheets.Add (name)
CreateWorksheet = name

End Function

'//////////////////////////////
'copy row to specified worksheet
Sub CopyToWorksheet(fromRow As Integer, toRow As Integer, name As String)

Dim i As Integer
Dim height As Integer
height = ThisWorkbook.Sheets(name).rows(toRow).RowHeight
For i = 1 To maxCol
     ThisWorkbook.Sheets(name).Cells(toRow, i).Value = ThisWorkbook.Sheets(mainSheetName).Cells(fromRow, i).Value
     ThisWorkbook.Sheets(name).Cells(toRow, i).WrapText = False
Next i

End Sub

'//////////////////////////////
'check if a worksheet for the current search already exists
'when looking up how to do this, this function was so complete
'and understandable, i copied it wholesale.  i forgot to write
'down where it came from in order to credit it
Function WorksheetExists(ByVal WorksheetName As String) As Boolean
Dim Sht As Worksheet
For Each Sht In ThisWorkbook.Worksheets
If Application.Proper(Sht.name) = Application.Proper(WorksheetName) Then
WorksheetExists = True
Exit Function
End If
Next Sht
WorksheetExists = False
End Function

'//////////////////////////////
'insures that text entered is an integer with a certain range
'the max boundary is optional, as i wasn't sure what i'd want as
'a max year cut off
Function DateDataCheck(data As String, tooLow As Integer, Optional tooHigh As Integer = 32767) As Boolean
If Not IsNumeric(data) Then
    DateDataCheck = False
    Exit Function
End If
If data = CInt(data) And data > tooLow And data < tooHigh Then
    DateDataCheck = True
Else
    DateDataCheck = False
End If

End Function

'//////////////////////////////
'performs the search-by-notes search
Function SearchNotesCheck(row As Integer) As Boolean

'returns true and exits the function if this search
'section is not active
If Not SearchCheckBox.Value Then
    'MsgBox ("search notes check box should be unchecked")
    SearchNotesCheck = True
    Exit Function
End If

Dim numberOfSearchTerms As Integer
numberOfSearchTerms = GetNumberOfSearchTerms

Select Case numberOfSearchTerms
   Case 1
        SearchNotesCheck = SearchForTerm(row, notesCol, searchBoxes(1).text)
   Case 2 To (UBound(searchBoxes)) 'was hardcoded as 5
        Dim operators() As String
        operators = GetLogicOperators(numberOfSearchTerms, boolBoxes)
        Dim searchBools() As Boolean
        ReDim searchBools(1 To numberOfSearchTerms)
        Dim i As Integer
        'collects the booleans for the search terms
        For i = 1 To numberOfSearchTerms
           searchBools(i) = SearchForTerm(row, notesCol, searchBoxes(i).text)
        Next i
        SearchNotesCheck = LogicCheck(searchBools, operators)
   Case Else
        SearchNotesCheck = False
        MsgBox ("number of search terms error" + vbCrLf + "it shouldn't be possible for you to get here")
End Select


End Function

'//////////////////////////////
'performs the search-in-column search
Function SearchColumnCheck(row As Integer) As Boolean

If Not ColumnSearchCheckBox.Value Then
    SearchColumnCheck = True
    Exit Function
End If
If ColumnBoolBox.ListIndex = 0 Then 'index 0 is 'in', 1 is 'not in'
    SearchColumnCheck = SearchForTerm(row, FindColNum(ColumnColBox.Value), ColumnTermBox.text)
Else
    SearchColumnCheck = Not SearchForTerm(row, FindColNum(ColumnColBox.Value), ColumnTermBox.text)
End If
End Function

'//////////////////////////////
'performs the before/after date check
Function SearchDateCheck(row As Integer) As Boolean

If Not DateCheckBox.Value Then
    'MsgBox ("date check box should be unchecked")
    SearchDateCheck = True
    Exit Function
End If

Dim collector As MatchCollection
Dim dateRegex As New Regexp
dateRegex.Pattern = "[01]?[0-9] *[-/] *([0-3]?[0-9] *[-/] *)?(19|20)?[0-9][0-9]"

'pulls the first date from current notes column
Set collector = dateRegex.Execute(ThisWorkbook.Sheets(mainSheetName).Cells(row, notesCol).text)
'returns false if there is no date
If collector.count = 0 Then
    SearchDateCheck = False
    Exit Function
End If

Dim dateString As String
Dim notesMonth As Integer
Dim notesYear As Integer
Dim inputMonth As Integer
Dim inputYear As Integer
inputMonth = CInt(DateMonthBox.text)
inputYear = CInt(DateYearBox.text)

dateString = collector(0)

'pulls the month from the date string
dateRegex.Pattern = "[01]?[0-9]"
Set collector = dateRegex.Execute(dateString)

notesMonth = CInt(collector(0))

'removes everything but the year from the date string
dateRegex.Pattern = "[01]?[0-9] *[-/] *([0-3]?[0-9] *[-/] *)?"
notesYear = SetCentury(CInt(dateRegex.Replace(dateString, "")))

Dim isAfter As Boolean
If notesYear > inputYear Then
    isAfter = True
ElseIf notesYear < inputYear Then
    isAfter = False
Else
    isAfter = notesMonth >= inputMonth
End If

If DateBoolBox.ListIndex = 0 Then
    SearchDateCheck = isAfter
Else
    SearchDateCheck = Not isAfter
End If

End Function

'//////////////////////////////
'formats the year when the box is exited
Private Sub DateYearBox_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Dim text As String
text = DateYearBox.text
If Not DateDataCheck(text, 0) Then
    DateYearBox.text = ""
Else
    DateYearBox.text = SetCentury(CInt(text))
End If

End Sub

'//////////////////////////////
'it was possible to exit the box with apparently unformatted data
'formats the year data when the frame is exited
Private Sub DateSearchFrame_Exit(ByVal Cancel As MSForms.ReturnBoolean)

Call DateYearBox_Exit(Cancel)

End Sub

'//////////////////////////////
'sets focus, depending on which search options are active
Sub SetAvailableFocus()

If SearchCheckBox.Value Then
    SearchBox1.SetFocus
ElseIf ColumnSearchCheckBox.Value Then
    ColumnTermBox.SetFocus
ElseIf DateCheckBox.Value Then
    DateMonthBox.SetFocus
End If

End Sub

'//////////////////////////////
'standardizes years in a four digit format.  two digit years 30 - 99 are placed in the 1900's
' years 00 - 29 are placed in 2000's.
Function SetCentury(year As Integer) As Integer

If year > 29 And year < 100 Then
    SetCentury = year + 1900
ElseIf year > -1 And year < 30 Then
    SetCentury = year + 2000
Else
SetCentury = year
End If

End Function


