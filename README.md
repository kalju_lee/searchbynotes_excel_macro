# Search By Notes Excel Macro #

I wrote this in 2015 while working as a clerk at the DCR.  There were two problems I was trying to address: the database was located in Boston and the time to query it was ridiculously long (probably still is).  Second, much of the useful information was in the 'Notes' section, which was not searchable.  The relevant part of the database could be downloaded into an excel spreadsheet, and then searched by macro.  
I've uploaded the macro code as txt files, along with an excel sheet that has the macro attached.

## Notes Search Overview ##

There are three types of searches, checking the box enables them.

**"Search Notes by Term"** only searches the notes.

**"Column Search"** can be used to check any column.

**"Search by Date"** compares against the first date in "notes".



**If more than one search type is used**, it will only return results that pass both searches.

**"Delete Searches"** button will delete all the sheets created by this program (and only the sheets created by this program).  Excel will prompt you at each sheet to ask if you're sure you want it deleted.  

**Each search will result in a new sheet** with a name based on the search criteria.  If the name of the sheet is too long, it will be truncated down to 31 characters, so it is possible that different searches will result in the same name for a sheet.  When that happens, the new search will overwrite the old one.

**Setting the search sheet** involves just typing in the sheet name and hitting the "Set Search Sheet" button.  If the sheet you are trying to search does not have a "notes" column, the program will close.  If you want to use this on a sheet without a "notes" column, put the word "notes" in a cell in the top row.


### How to search a spreadsheet ###
The sheet you want to conduct searches in needs to be copied into this workbook in order for the search function to not freak out.
Once the sheet is successfully copied over, it should say the name of this workbook up on top ("SearchByNotesDatabase.xlsm").
If the sheet you're searching isn't named "UA_Home_Version", you'll need to set the search sheet to the one you're using (see "Setting the search sheet").


### More on "Search Notes by Term" ###

A maximum of five terms can be searched for.  Any lines left blank are not searched.  

If a search line left blank, the program will ignore search terms that are under it.

If a logic operator (the drop down menu of And, Not, Or, Xor) is left blank, it is considered treated as "And".

Each search term is resolved before moving on to the next, so it is not possible to do something like: 
(x And y) Or (a And b)
instead you would get:
(((x And y) Or a) And b)
Hopefully that didn't just make it more confusing.

"Or" means "one or the other or both."

"Xor" means "one or the other but NOT both."


### More on "Column Search" ###

"Column Search" can be used to return results that either have or do not have something in a particular column.

Like "Search Notes By Term", this cannot be used to look for blank cells.

A cell being blank is not the same as having a zero.  Maybe that's obvious, but I forgot it and got unexpected search results once.


### More on "Search by Date" ###

"Search by Date" checks the given date against the first date found in the "notes" column.  

Two digit years from 00 to 29 are changed to 20xx, 30-99 are changed to 19xx.

This search will not return any rows that don't have a date in the notes.

Dates where the month is written out will not be recognized by this search.